#include "map.h"

#define N 24

int main(void) {
    map_t map = {0};
    for (size_t i = 1; i < N; i++) {
        map_put(&map, (void *)i, (void *)(i+1));
    }

    for (size_t i = 1; i < N; i++) {
        void *val = map_get(&map, (void *)i);
        printf("%lu\n", (size_t)val);
    }

	printf("Length: %zu\n", map.len);
    map_remove(&map, (const void*)20);
	map_put(&map, (const void*) 20, (void*)20);
	printf("Length: %zu\n", map.len);

	map_destroy(&map);
    return 0;
}

