default: all

CFLAGS=-std=c99 -g -Wall
LIBS=-lm
EXE=out

all: main.o map.o
	gcc $(CFLAGS) $^ -o $(EXE) $(LIBS)

%.o: %.c
	gcc $(CFLFAGS) -c $^

.PHONY: clean

clean:
	-rm -f *.o $(EXE)
