#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

void *xcalloc(size_t num_elems, size_t elem_size) {
    void *ptr = calloc(num_elems, elem_size);
    if (!ptr) {
        perror("xcalloc failed");
        exit(1);
    }
    return ptr;
}

void *xrealloc(void *ptr, size_t num_bytes) {
    ptr = realloc(ptr, num_bytes);
    if (!ptr) {
        perror("xrealloc failed");
        exit(1);
    }
    return ptr;
}

void *xmalloc(size_t num_bytes) {
    void *ptr = malloc(num_bytes);
    if (!ptr) {
        perror("xmalloc failed");
        exit(1);
    }
    return ptr;
}

void *memdup(void *src, size_t size) {
    void *dest = xmalloc(size);
    memcpy(dest, src, size);
    return dest;
}

uint64_t hash_uint64(uint64_t x) {
    x *= 0xff51afd7ed558ccd;
    x ^= x >> 32;
    return x;
}

uint64_t hash_ptr(const void *ptr) {
    return hash_uint64((uintptr_t)ptr);
}

uint64_t hash_mix(uint64_t x, uint64_t y) {
    x ^= y;
    x *= 0xff51afd7ed558ccd;
    x ^= x >> 32;
    return x;
}

uint64_t hash_bytes(const void *ptr, size_t len) {
    uint64_t x = 0xcbf29ce484222325;
    const char *buf = (const char *)ptr;
    for (size_t i = 0; i < len; i++) {
        x ^= buf[i];
        x *= 0x100000001b3;
        x ^= x >> 32;
    }
    return x;
}

void *map_get(map_t *map, const void *key) {
    if (map->len == 0) {
        return NULL;
    }
    assert(IS_POW2(map->cap));
    size_t i = (size_t)hash_ptr(key);
    assert(map->len < map->cap);
    for (;;) {
        i &= map->cap - 1;
        if (map->keys[i] == key) {
            return map->vals[i];
        } else if (!map->keys[i]) {
            return NULL;
        }
        i++;
    }
    return NULL;
}

void map_remove(map_t *map, const void* key) {
    size_t i = (size_t)hash_ptr(key);
    i &= map->cap-1;
    if (map->keys[i]) {
        map->keys[i] = NULL;
        map->vals[i] = NULL;
		map->len--;
    }
}

void map_grow(map_t *map, size_t new_cap) {
    new_cap = CLAMP_MIN(new_cap, 16);
    map_t new_map = {
        .keys = xcalloc(new_cap, sizeof(void *)),
        .vals = xmalloc(new_cap * sizeof(void *)),
        .cap = new_cap,
    };
    for (size_t i = 0; i < map->cap; i++) {
        if (map->keys[i]) {
            map_put(&new_map, map->keys[i], map->vals[i]);
        }
    }
    free((void *)map->keys);
    free(map->vals);
    *map = new_map;
}

void map_put(map_t *map, const void *key, void *val) {
    assert(key);
    assert(val);
    if (2*map->len >= map->cap) {
        map_grow(map, 2*map->cap);
    }
    assert(2*map->len < map->cap);
    assert(IS_POW2(map->cap));
    size_t i = (size_t)hash_ptr(key);
    for (;;) {
        i &= map->cap - 1;
        if (!map->keys[i]) {
            map->keys[i] = key;
            map->vals[i] = val;
            map->len++;
            return;
        } else if (map->keys[i] == key) {
            map->vals[i] = val;
            return;
        }
        i++;
    }
}

void map_destroy(map_t *map) {
	assert(map->keys != NULL);
	assert(map->vals != NULL);
	free(map->keys);
	free(map->vals);
}

