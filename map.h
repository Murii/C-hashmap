#ifndef _MAP_H
#define _MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#ifndef MIN
#define MIN(x, y) ((x) <= (y) ? (x) : (y))
#endif
#ifndef MAX
#define MAX(x, y) ((x) >= (y) ? (x) : (y))
#endif
#ifndef CLAMP_MAX
#define CLAMP_MAX(x, max) MIN(x, max)
#endif
#ifndef CLAMP_MIN
#define CLAMP_MIN(x, min) MAX(x, min)
#endif
#ifndef IS_POW2
#define IS_POW2(x) (((x) != 0) && ((x) & ((x)-1)) == 0)
#endif

typedef struct {
    const void **keys;
    void **vals;
    size_t len;
    size_t cap;
} map_t;

void *map_get(map_t *map, const void *key);
void map_remove(map_t *map, const void* key);
void map_grow(map_t *map, size_t new_cap);
void map_put(map_t *map, const void *key, void *val);
void map_destroy(map_t *map);
#endif
